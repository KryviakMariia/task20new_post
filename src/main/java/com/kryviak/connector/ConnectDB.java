package com.kryviak.connector;

import com.kryviak.filepath.PathDB;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

public class ConnectDB {
    private static Logger logger = LogManager.getLogger(ConnectDB.class);
    private Connection connection = null;
    private Statement statement = null;
    private ResultSet resultSet = null;
    private PreparedStatement preparedStatement = null;
    private PathDB pathDB = new PathDB();

    public Connection connectToDB() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            connection = DriverManager.getConnection(
                    pathDB.propertyFile("url"),
                    pathDB.propertyFile("user"),
                    pathDB.propertyFile("password"));
        } catch (SQLException | ClassNotFoundException ex) {
            logger.error(ex);
        }
        return connection;
    }

    public Connection closeConnections() {
        if (resultSet != null) try {
            resultSet.close();
        } catch (SQLException e) {
            logger.error(e);
        }
        if (statement != null) try {
            statement.close();
        } catch (SQLException e) {
            logger.error(e);
        }
        if (connection != null) try {
            connection.close();
        } catch (SQLException e) {
            logger.error(e);
        }
        if (preparedStatement != null) try {
            preparedStatement.close();
        } catch (SQLException e) {
            logger.error(e);
        }
        return null;
    }
}
