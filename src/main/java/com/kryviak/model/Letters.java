package com.kryviak.model;

import java.util.Date;

public class Letters {
    private int id_letters;
    private int price;
    private String date;

    public Letters(int id_letters, int price, String date) {
        this.id_letters = id_letters;
        this.price = price;
        this.date = date;
    }

    public int getId_letters() {
        return id_letters;
    }

    public int getPrice() {
        return price;
    }

    public String getDate() {
        return date;
    }

    @Override
    public String toString() {
        return String.format("id: %-5s price: %-5s date: %-5s",
                id_letters, price, date);
    }
}
