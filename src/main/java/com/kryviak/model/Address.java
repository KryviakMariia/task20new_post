package com.kryviak.model;

public class Address {
    private int id_address;
    private String streetOrAvenue;
    private int house_number;
    private String city;

    public Address(int id_address, String streetOrAvenue, int house_number, String city) {
        this.id_address = id_address;
        this.streetOrAvenue = streetOrAvenue;
        this.house_number = house_number;
        this.city = city;
    }

    public int getId_address() {
        return id_address;
    }

    public String getStreetOrAvenue() {
        return streetOrAvenue;
    }


    public int getHouse_number() {
        return house_number;
    }

    public String getCity() {
        return city;
    }

    @Override
    public String toString() {
        return String.format("id: %-5s streetOrAvenue: %-15s house_number: %-5s city: %-5s",
                id_address, streetOrAvenue, house_number, city);
    }
}
