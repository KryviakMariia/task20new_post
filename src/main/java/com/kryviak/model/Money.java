package com.kryviak.model;

import java.util.Date;

public class Money {
    private int id_money;
    private int sum;
    private int price;
    private String date;

    public Money(int id_money, int sum, int price, String date) {
        this.id_money = id_money;
        this.sum = sum;
        this.price = price;
        this.date = date;
    }

    public int getId_money() {
        return id_money;
    }

    public int getSum() {
        return sum;
    }

    public int getPrice() {
        return price;
    }

    public String getDate() {
        return date;
    }

    @Override
    public String toString() {
        return String.format("id: %-5s sum: %-5s price: %-5s date: %-5s",
                id_money, sum, price, date);
    }
}
