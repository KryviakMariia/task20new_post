package com.kryviak.model;

public class Passport {
    private int id_passport;
    private String series;
    private int number;

    public Passport(int id_passport, String series, int number) {
        this.id_passport = id_passport;
        this.series = series;
        this.number = number;
    }

    public int getId_passport() {
        return id_passport;
    }

    public String getSeries() {
        return series;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return String.format("id: %-5s series: %-5s number: %-5s",
                id_passport, series, number);
    }
}
