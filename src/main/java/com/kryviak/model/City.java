package com.kryviak.model;

public class City {
    private int id_city;
    private String city;;

    public City(int id_city, String city) {
        this.id_city = id_city;
        this.city = city;
    }

    @Override
    public String toString() {
        return String.format("id: %-5s city: %-5s",
                id_city, city);
    }
}
