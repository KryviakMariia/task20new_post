package com.kryviak.model;

import java.util.Date;

public class Box {
    private int id_box;
    private String weight;
    private int price;
    private String  date;

    public Box(int id_box, String weight, int price, String date) {
        this.id_box = id_box;
        this.weight = weight;
        this.price = price;
        this.date = date;
    }

    public int getId_box() {
        return id_box;
    }

    public String getWeight() {
        return weight;
    }

    public int getPrice() {
        return price;
    }

    public String getDate() {
        return date;
    }

    @Override
    public String toString() {
        return String.format("id: %-5s weight: %-5s price: %-5s date: %-5s",
                id_box, weight, price, date);
    }
}
