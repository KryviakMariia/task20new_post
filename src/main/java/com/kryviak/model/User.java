package com.kryviak.model;

import java.util.Date;

public class User {
    private int id_user;
    private String surname;
    private String name;
    private String date_of_birth;
    private int passport_id;
    private int address_id;
    private int phone_number;

    public User(int id_user, String surname, String name, String date_of_birth,
                int passport_id, int address_id, int phone_number) {
        this.id_user = id_user;
        this.surname = surname;
        this.name = name;
        this.date_of_birth = date_of_birth;
        this.passport_id = passport_id;
        this.address_id = address_id;
        this.phone_number = phone_number;
    }

    public int getId_user() {
        return id_user;
    }

    @Override
    public String toString() {
        return String.format("id: %-5s surname: " +
                "%-10s name: %-10s date_of_birth: %-5s passport_id: " +
                "%-5s phone_number: %-5s address_id: %-5s",
                id_user, surname, name, date_of_birth, phone_number, address_id, passport_id);
    }
}
