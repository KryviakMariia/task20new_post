package com.kryviak.model;

public class Sending {
    private int id_sending;
    private int parcel_id;
    private int id_address_from;
    private int id_address_to;
    private int user_from;
    private int user_to;

    public Sending(int id_sending, int parcel_id, int id_address_from, int user_from, int id_address_to, int user_to) {
        this.id_sending = id_sending;
        this.parcel_id = parcel_id;
        this.id_address_from = id_address_from;
        this.id_address_to = id_address_to;
        this.user_from = user_from;
        this.user_to = user_to;
    }

    @Override
    public String toString() {
        return String.format("id: %-5s parcel_id: %-5s id_address_from: %-5s user_from: %-5s id_address_to: %-5s user_to: %-5s",
                id_sending, parcel_id, id_address_from, user_from, id_address_to, user_to);
    }
}
