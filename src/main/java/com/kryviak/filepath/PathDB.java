package com.kryviak.filepath;

import com.kryviak.dao.implementation.UserDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PathDB {
    private static Logger logger = LogManager.getLogger(UserDAO.class);
    private Properties prop = new Properties();
    private InputStream input = null;
    private String filePath = null;

    public String propertyFile(String keyToFile) {
        try {
            String pathToFile = "connectToDB.properties";
            input = getClass().getClassLoader().getResourceAsStream(pathToFile);
            prop.load(input);
            filePath = prop.getProperty(keyToFile);
        } catch (
                IOException ex) {
            logger.error(ex);
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    logger.error(e);
                }
            }
        }
        return filePath;
    }
}