package com.kryviak.dao;

import java.sql.Connection;

public interface AddressDAOi {
    void addAddress(Connection connection);
    int getAddressId(Connection connection);
}
