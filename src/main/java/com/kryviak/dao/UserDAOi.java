package com.kryviak.dao;

import com.kryviak.model.User;

import java.sql.Connection;

public interface UserDAOi {
    void addNewUser(Connection connection);

    int getUserId(Connection connection);
}
