package com.kryviak.dao.implementation;

import com.kryviak.dao.implementation.constant.QueriesDB;
import com.kryviak.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.Scanner;

public class UserDAO implements com.kryviak.dao.UserDAOi {
    private static Logger logger = LogManager.getLogger(UserDAO.class);
    private PassportDAO passportDAO = new PassportDAO();
    private AddressDAO addressDAO = new AddressDAO();
    private Scanner input = new Scanner(System.in);
    private String surname;
    private String name;
    private String dateOfBirth;
    private int passportId;
    private int phoneNumber;
    private int addressID;

    @Override
    public void addNewUser(Connection connection) {
        inputUserData();
        logger.info("Please input phone number: ");
        phoneNumber = input.nextInt();
        passportId = passportDAO.addPassportData(connection);
        addressID = addressDAO.getAddressId(connection);
        addUserToDB(connection);
        User user = new User(getUserId(connection), surname, name, dateOfBirth, passportId, phoneNumber, addressDAO.getAddressId(connection));
        logger.info(user.toString());
    }

    public boolean addUserToDB(Connection connection) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(QueriesDB.INSERT_USER.getQueries());
            preparedStatement.setString(1, surname);
            preparedStatement.setString(2, name);
            preparedStatement.setString(3, dateOfBirth);
            preparedStatement.setInt(4, passportId);
            preparedStatement.setInt(5, addressID);
            preparedStatement.setInt(6, phoneNumber);
            int n = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
            return false;
        }
        return true;
    }

    private void inputUserData() {
        logger.info("Please input a user surname: ");
        surname = input.next();
        logger.info("Please input a user name: ");
        name = input.next();
        logger.info("Please input date of birth: ");
        dateOfBirth = input.next();
    }

    public int getUserId(Connection connection) {
        int idDB = 0;
        String surnameInDB;
        String nameInDB;
        String dateOfBirthDB;
        inputUserData();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(QueriesDB.GET_ALL_USER.getQueries());
            while (resultSet.next()) {
                idDB = resultSet.getInt("id");
                surnameInDB = resultSet.getString("surname");
                nameInDB = resultSet.getString("name");
                dateOfBirthDB = resultSet.getString("date_of_birth");
                if (surnameInDB.equals(surname) && nameInDB.equals(name) && dateOfBirthDB.equals(dateOfBirth)) {
                    return idDB;
                }
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return idDB;
    }
}
