package com.kryviak.dao.implementation;

import com.kryviak.dao.CityDAOi;
import com.kryviak.dao.implementation.constant.QueriesDB;
import com.kryviak.model.City;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.Scanner;

public class CityDAO implements CityDAOi {
    private static Logger logger = LogManager.getLogger(CityDAO.class);
    private Scanner input = new Scanner(System.in);
    private String city;

    public void addCity(Connection connection) {
        city = inputCity();
        addCityToDB(connection);
        City city1 = new City(getCityIdDB(connection), city);
        logger.info(city1.toString());
    }

    public boolean addCityToDB(Connection connection) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(QueriesDB.INSERT_CITY.getQueries());
            preparedStatement.setString(1, city);
            int n = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
            return false;
        }
        return true;
    }

    private String inputCity() {
        logger.info("Please input city name: ");
        return input.next();
    }

    private int getCityIdDB(Connection connection) {
        int idDB = 0;
        String cityDB;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(QueriesDB.GET_ALL_CITY.getQueries());
            while (resultSet.next()) {
                idDB = resultSet.getInt("id");
                cityDB = resultSet.getString("city");
                if (cityDB.equals(city)) {
                    return idDB;
                }
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return idDB;
    }
}
