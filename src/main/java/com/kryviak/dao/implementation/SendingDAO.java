package com.kryviak.dao.implementation;

import com.kryviak.dao.SendingDAOi;
import com.kryviak.dao.implementation.constant.QueriesDB;
import com.kryviak.model.Sending;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.Scanner;

public class SendingDAO implements SendingDAOi {
    private static Logger logger = LogManager.getLogger(SendingDAO.class);
    private Scanner input = new Scanner(System.in);
    private AddressDAO addressDAO = new AddressDAO();
    private UserDAO userDAO = new UserDAO();
    private MoneyDAO moneyDAO = new MoneyDAO();
    private LettersDAO lettersDAO = new LettersDAO();
    private BoxDAO boxDAO = new BoxDAO();
    private int addressIdFrom;
    private int userIdFrom;
    private int addressIdTo;
    private int userIdTo;
    private int parcelId;
    private String stageOfParcel;

    public void sentParcel(Connection connection) {
        PreparedStatement preparedStatement = null;
        int choose = chooseOperation();
        logger.info("From");
        addressIdFrom = addressDAO.getAddressId(connection);
        userIdFrom = userDAO.getUserId(connection);
        logger.info("To");
        addressIdTo = addressDAO.getAddressId(connection);
        userIdTo = userDAO.getUserId(connection);
        parcelId = returnIdOfParcel(connection, choose);
        stageOfParcel = inputStageOfSend();
        try {
            preparedStatement = chooseParcel(connection, choose);
        } catch (SQLException e) {
            logger.error(e);
        }
        addParcelToDB(preparedStatement);
        Sending sending = new Sending(returnId(connection, addressIdFrom, userIdFrom, addressIdTo, userIdTo),
                parcelId, addressIdFrom, userIdFrom, addressIdTo, userIdTo);
        logger.info(sending.toString());
    }

    public boolean addParcelToDB(PreparedStatement preparedStatement) {
        try {
            preparedStatement.setInt(1, parcelId);
            preparedStatement.setInt(2, addressIdFrom);
            preparedStatement.setInt(4, userIdFrom);
            preparedStatement.setInt(3, addressIdTo);
            preparedStatement.setInt(5, userIdTo);
            preparedStatement.setString(6, stageOfParcel);
            int n = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
            return false;
        }
        return true;
    }

    private String inputStageOfSend() {
        logger.info("Please input stage of parcel\n'sent' or 'waiting_for_recipient' or 'received' or'redirected'");
        return input.next();
    }

    private int chooseOperation() {
        logger.info("Please choose parcel:" +
                "\n'1' send money\n'2' send letters\n'3' send box");
        return input.nextInt();
    }

    private PreparedStatement chooseParcel(Connection connection, int typeOfParcel) throws SQLException {
        PreparedStatement preparedStatement = null;

        switch (typeOfParcel) {
            case 1:
                preparedStatement = connection.prepareStatement(QueriesDB.INSERT_SENDING_MONEY.getQueries());
                break;
            case 2:
                preparedStatement = connection.prepareStatement(QueriesDB.INSERT_SENDING_LETTERS.getQueries());
                break;
            case 3:
                preparedStatement = connection.prepareStatement(QueriesDB.INSERT_SENDING_BOX.getQueries());
                break;
        }
        return preparedStatement;
    }

    private int returnIdOfParcel(Connection connection, int typeOfParcel) {
        int id = 0;
        switch (typeOfParcel) {
            case 1:
                id = moneyDAO.addMoney(connection);
                break;
            case 2:
                id = lettersDAO.addLetters(connection);
                break;
            case 3:
                id = boxDAO.addBox(connection);
                break;
        }
        return id;
    }

    private int returnId(Connection connection, int addressIdFrom, int userIdFrom, int addressIdTo, int userIdTo) {
        int idDB = 0;
        int addressIdFromDB;
        int userIdFromDB;
        int addressIdToDB;
        int userIdToDB;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(QueriesDB.GET_ALL_SENDING.getQueries());
            while (resultSet.next()) {
                idDB = resultSet.getInt("id");
                addressIdFromDB = resultSet.getInt("id_address_from");
                userIdFromDB = resultSet.getInt("user_id_from");
                addressIdToDB = resultSet.getInt("id_address_to");
                userIdToDB = resultSet.getInt("user_id_to");
                if (addressIdFrom == addressIdFromDB && userIdFrom == userIdFromDB && addressIdTo == addressIdToDB && userIdTo == userIdToDB) {
                    return idDB;
                }
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return idDB;
    }
}
