package com.kryviak.dao.implementation;

import com.kryviak.dao.AddressDAOi;
import com.kryviak.dao.implementation.constant.QueriesDB;
import com.kryviak.model.Address;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.Scanner;

public class AddressDAO implements AddressDAOi {
    private static Logger logger = LogManager.getLogger(AddressDAO.class);
    private Scanner input = new Scanner(System.in);
    private String street;
    private String city;
    private int numberOfHouse;

    public void addAddress(Connection connection) {
        inputData();
        addAddressToDB(connection);
        Address address = new Address(getAddressId(connection), street, numberOfHouse, city);
        logger.info("New address '" + address.toString() + "' was added successfully");
    }

    public boolean addAddressToDB(Connection connection) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(QueriesDB.INSERT_ADDRESS.getQueries());
            preparedStatement.setString(1, street);
            preparedStatement.setInt(2, numberOfHouse);
            preparedStatement.setString(3, city);
            int n = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
            return false;
        }
        return true;
    }

    private void inputData() {
        logger.info("Please input street name: ");
        street = input.next();
        logger.info("Please input house number: ");
        numberOfHouse = input.nextInt();
        logger.info("Please input city name: ");
        city = input.next();
    }

    public int getAddressId(Connection connection) {
        inputData();
        int idDB = 0;
        String streetInDB;
        String cityInDB;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(QueriesDB.GET_ALL_ADDRESS.getQueries());
            while (resultSet.next()) {
                idDB = resultSet.getInt("id");
                streetInDB = resultSet.getString("street");
                cityInDB = resultSet.getString("city");
                if (street.equals(streetInDB) && city.equals(cityInDB)) {
                    return idDB;
                }
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return idDB;
    }
}
