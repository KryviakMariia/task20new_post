package com.kryviak.dao.implementation;

import com.kryviak.dao.LettersDAOi;
import com.kryviak.dao.implementation.constant.QueriesDB;
import com.kryviak.model.Letters;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class LettersDAO implements LettersDAOi {
    private static Logger logger = LogManager.getLogger(LettersDAO.class);
    private Scanner input = new Scanner(System.in);
    private int price;
    private String date;

    public int addLetters(Connection connection) {
        inputValueOfPrice();
        date = getActualDate();
        addLettersToDB(connection);
        Letters letters = new Letters(getLettersIdDB(connection), price, date);
        logger.info(letters.toString());
        return letters.getId_letters();
    }

    public boolean addLettersToDB(Connection connection) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(QueriesDB.INSERT_LETTERS.getQueries());
            preparedStatement.setInt(1, price);
            preparedStatement.setString(2, date);
            int n = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
            return false;
        }
        return true;
    }

    private int getLettersIdDB(Connection connection) {
        int idDB = 0;
        int priceDB;
        String dateDB;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(QueriesDB.GET_ALL_LETTERS.getQueries());
            while (resultSet.next()) {
                idDB = resultSet.getInt("id");
                priceDB = resultSet.getInt("price");
                dateDB = resultSet.getString("date");
                if (priceDB == price && dateDB.equals(date)) {
                    return idDB;
                }
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return idDB;
    }

    private void inputValueOfPrice() {
        logger.info("Please input sum that user need to pay: ");
        price = input.nextInt();
    }

    private String getActualDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.now();
        return dtf.format(localDate);
    }
}
