package com.kryviak.dao.implementation.constant;

public enum QueriesDB {
    INSERT_USER("INSERT INTO new_post.user (surname, name, date_of_birth, passport_id, address_id, phone_number) VALUE (?, ?, ?, ?, ?, ?)"),
    GET_ALL_USER("SELECT * FROM user"),

    INSERT_CITY("INSERT INTO new_post.city (city) VALUE (?)"),
    GET_ALL_CITY("SELECT * FROM city"),

    INSERT_PASSPORT("INSERT INTO new_post.passport (series, number) VALUE (?, ?)"),
    GET_ALL_PASSPORT("SELECT * FROM passport"),

    INSERT_ADDRESS("INSERT INTO new_post.address (street, number_of_street, city) VALUE (?, ?, ?)"),
    GET_ALL_ADDRESS("SELECT * FROM address"),

    INSERT_SENDING_MONEY("INSERT INTO new_post.sending (money_id, id_address_from, id_address_to, user_id_from, user_id_to, stage) VALUE (?, ?, ?, ?, ?, ?)"),
    INSERT_SENDING_LETTERS("INSERT INTO new_post.sending (letters_id, id_address_from, id_address_to, user_id_from, user_id_to, stage) VALUE (?, ?, ?, ?, ?, ?)"),
    INSERT_SENDING_BOX("INSERT INTO new_post.sending (box_id, id_address_from, id_address_to, user_id_from, user_id_to, stage) VALUE (?, ?, ?, ?, ?, ?)"),
    GET_ALL_SENDING("SELECT * FROM sending"),

    INSERT_MONEY("INSERT INTO new_post.money (sum, price, date) VALUE (?, ?, ?)"),
    GET_ALL_MONEY("SELECT * FROM money"),

    INSERT_LETTERS("INSERT INTO new_post.letters (price, date) VALUE (?, ?)"),
    GET_ALL_LETTERS("SELECT * FROM letters"),

    INSERT_BOX("INSERT INTO new_post.box (weight, price, date) VALUE (?, ?, ?)"),
    GET_ALL_BOX("SELECT * FROM box");

    private String queries;

    QueriesDB(String queries) {
        this.queries = queries;
    }

    public String getQueries() {
        return queries;
    }
    }
