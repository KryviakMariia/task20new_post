package com.kryviak.dao.implementation;

import com.kryviak.dao.BoxDAOi;
import com.kryviak.dao.implementation.constant.QueriesDB;
import com.kryviak.model.Box;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class BoxDAO implements BoxDAOi {
    private static Logger logger = LogManager.getLogger(BoxDAO.class);
    private Scanner input = new Scanner(System.in);
    private String weight;
    private int price;
    private String date;
    private int id;

    public int addBox(Connection connection) {
        inputValueOfPrice();
        date = getActualDate();
        addBoxToDB(connection);
        Box box = new Box(getBoxIdDB(connection), weight, price, date);
        logger.info(box.toString());
        return box.getId_box();
    }

    public boolean addBoxToDB(Connection connection) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(QueriesDB.INSERT_BOX.getQueries());
            preparedStatement.setString(1, weight);
            preparedStatement.setInt(2, price);
            preparedStatement.setString(3, date);
            int n = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
            return false;
        }
        return true;
    }

    private int getBoxIdDB(Connection connection) {
        int idDB = 0;
        String weightDB;
        int priceDB;
        String dateDB;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(QueriesDB.GET_ALL_BOX.getQueries());
            while (resultSet.next()) {
                idDB = resultSet.getInt("id");
                weightDB = resultSet.getString("weight");
                priceDB = resultSet.getInt("price");
                dateDB = resultSet.getString("date");
                if (priceDB == price && dateDB.equals(date) && weightDB.equals(weight)) {
                    return idDB;
                }
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return idDB;
    }

    private void inputValueOfPrice() {
        logger.info("Please input sum that user need to pay: ");
        price = input.nextInt();
        logger.info("Please input weight of parcel \nto5kg' or 'to10kg' or 'to50kg': ");
        weight = input.next();
    }

    private String getActualDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.now();
        return dtf.format(localDate);
    }
}
