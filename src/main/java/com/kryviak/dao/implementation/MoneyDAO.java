package com.kryviak.dao.implementation;

import com.kryviak.dao.MoneyDAOi;
import com.kryviak.dao.implementation.constant.QueriesDB;
import com.kryviak.model.Money;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class MoneyDAO implements MoneyDAOi {
    private static Logger logger = LogManager.getLogger(MoneyDAO.class);
    private Scanner input = new Scanner(System.in);
    private int sum;
    private int price;
    private String date;

    public int addMoney(Connection connection) {
        inputValueOfMoney();
        date = getActualDate();
        addMoneyToDB(connection);
        Money money = new Money(getMoneyIdDB(connection), sum, price, date);
        logger.info(money.toString());
        return money.getId_money();
    }

    public boolean addMoneyToDB(Connection connection) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(QueriesDB.INSERT_MONEY.getQueries());
            preparedStatement.setInt(1, sum);
            preparedStatement.setInt(2, price);
            preparedStatement.setString(3, date);
            int n = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
            return false;
        }
        return true;
    }

    private int getMoneyIdDB(Connection connection) {
        int idDB = 0;
        int sumDB;
        int priceDB;
        String dateDB;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(QueriesDB.GET_ALL_MONEY.getQueries());
            while (resultSet.next()) {
                idDB = resultSet.getInt("id");
                sumDB = resultSet.getInt("sum");
                priceDB = resultSet.getInt("price");
                dateDB = resultSet.getString("date");
                if (sumDB == sum && priceDB == price && dateDB.equals(getActualDate())) {
                    return idDB;
                }
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return idDB;
    }

    private void inputValueOfMoney() {
        logger.info("Please input sum that you want to sent: ");
        sum = input.nextInt();
        logger.info("Please input sum that user need to pay: ");
        price = input.nextInt();
    }

    private String getActualDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.now();
        return dtf.format(localDate);
    }
}
