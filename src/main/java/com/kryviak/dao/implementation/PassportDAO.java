package com.kryviak.dao.implementation;

import com.kryviak.dao.PassportDAOi;
import com.kryviak.dao.implementation.constant.QueriesDB;
import com.kryviak.model.Passport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.Scanner;

public class PassportDAO implements PassportDAOi {
    private static Logger logger = LogManager.getLogger(PassportDAO.class);
    private Scanner input = new Scanner(System.in);
    private String series;
    private int numberOfPassport;


    public int addPassportData(Connection connection) {
        logger.info("Please input a passport series: ");
        series = input.next();
        logger.info("Please input passport number: ");
        numberOfPassport = input.nextInt();
        addPassportToDB(connection);
        Passport passport = new Passport(getPassportID(connection), series, numberOfPassport);
        logger.info(passport.toString());
        return passport.getId_passport();
    }

    public boolean addPassportToDB(Connection connection) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(QueriesDB.INSERT_PASSPORT.getQueries());
            preparedStatement.setString(1, series);
            preparedStatement.setInt(2, numberOfPassport);
            int n = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            logger.error(e);
            return false;
        }
        return true;
    }

    private int getPassportID(Connection connection) {
        int idDB = 0;
        String seriesInDB;
        int numberInDB;
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(QueriesDB.GET_ALL_PASSPORT.getQueries());
            while (resultSet.next()) {
                idDB = resultSet.getInt("id");
                seriesInDB = resultSet.getString("series");
                numberInDB = resultSet.getInt("number");
                if (seriesInDB.equals(series) && numberOfPassport == numberInDB) {
                    return idDB;
                }
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return idDB;
    }
}
