package com.kryviak.view;

import com.kryviak.connector.ConnectDB;
import com.kryviak.dao.implementation.AddressDAO;
import com.kryviak.dao.implementation.CityDAO;
import com.kryviak.dao.implementation.SendingDAO;
import com.kryviak.dao.implementation.UserDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Menu {
    private static Logger logger = LogManager.getLogger(Menu.class);
    private UserDAO userDAO = new UserDAO();
    private ConnectDB connectDB = new ConnectDB();
    private SendingDAO sendingDAO = new SendingDAO();
    private CityDAO cityDAO = new CityDAO();
    private AddressDAO addressDAO = new AddressDAO();

    private static final int CLEAR_MENU_VALUE = 6;

    private Scanner input = new Scanner(System.in);
    private int selection;
    private boolean flag = false;

    public void printMenu() {
        while (!flag || selection != 0) {
            logger.info("Choose operation: \n'1' add new user\n'2' sent parcel\n'3' add city\n'4' add address of new post\n'0' exit");
            try {
                selection = input.nextInt();
                flag = true;
            } catch (InputMismatchException e) {
                logger.info("Please input only number value");
                selection = CLEAR_MENU_VALUE;
                String flush = input.next();
            }
            chooseOperation();
        }
        connectDB.closeConnections();
    }

    private void chooseOperation() {
        switch (selection) {
            case 1:
                userDAO.addNewUser(connectDB.connectToDB());
                break;
            case 2:
                sendingDAO.sentParcel(connectDB.connectToDB());
                break;
            case 3:
                cityDAO.addCity(connectDB.connectToDB());
                break;
            case 4:
                addressDAO.addAddress(connectDB.connectToDB());
                break;
        }
    }
}
