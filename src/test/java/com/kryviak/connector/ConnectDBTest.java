package com.kryviak.connector;

import org.junit.Assert;
import org.junit.Test;

import java.sql.*;

public class ConnectDBTest {
    private ConnectDB connectDB = new ConnectDB();
    private Connection connection = null;

    @Test
    public void connect() {
        connection = connectDB.connectToDB();
        Assert.assertNotNull("No connect to BD", connection);
    }

    @Test
    public void closeConnect() throws SQLException {
        connection = connectDB.connectToDB();
        connection = connectDB.closeConnections();
        Assert.assertNull("Connect to DB is still open", connection);
    }
}
