package com.kryviak.dao;

import com.kryviak.dao.implementation.UserDAO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserDAOTest {
    @InjectMocks
    UserDAO userDAO;
    @Mock
    Connection connection;
    @Mock
    PreparedStatement stmt;

    @Before
    public void setUp() throws SQLException {
        when(connection.prepareStatement(eq("INSERT INTO new_post.user (surname, name, date_of_birth, passport_id, address_id, phone_number) VALUE (?, ?, ?, ?, ?, ?)"))).thenReturn(stmt);
        when(stmt.executeUpdate()).thenReturn(1);
    }

    @Test
    public void testInsertTable() {
        Assert.assertTrue(userDAO.addUserToDB(connection));
    }
}
