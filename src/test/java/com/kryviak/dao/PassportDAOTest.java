package com.kryviak.dao;

import com.kryviak.dao.implementation.PassportDAO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PassportDAOTest {
    @InjectMocks
    PassportDAO passportDAO;
    @Mock
    Connection connection;
    @Mock
    PreparedStatement stmt;

    @Before
    public void setUp() throws SQLException {
        when(connection.prepareStatement(eq("INSERT INTO new_post.passport (series, number) VALUE (?, ?)"))).thenReturn(stmt);
        when(stmt.executeUpdate()).thenReturn(1);
    }

    @Test
    public void testInsertTable() {
        Assert.assertTrue(passportDAO.addPassportToDB(connection));
    }
}
