package com.kryviak.dao;

import com.kryviak.dao.implementation.AddressDAO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AddressDAOTest {
    @InjectMocks
    AddressDAO addressDAO;
    @Mock
    Connection connection;
    @Mock
    PreparedStatement stmt;

    @Before
    public void setUp() throws SQLException {
        when(connection.prepareStatement(eq("INSERT INTO new_post.address (street, number_of_street, city) VALUE (?, ?, ?)"))).thenReturn(stmt);
        when(stmt.executeUpdate()).thenReturn(1);
    }

    @Test
    public void testInsertTable() {
        Assert.assertTrue(addressDAO.addAddressToDB(connection));
    }
}
